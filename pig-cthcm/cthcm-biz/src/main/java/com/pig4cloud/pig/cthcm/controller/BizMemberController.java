/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.cthcm.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.cthcm.entity.BizMember;
import com.pig4cloud.pig.cthcm.service.BizMemberService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 客户表
 *
 * @author pig code generator
 * @date 2021-08-16 16:10:15
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/biz/member" )
@Api(value = "bizmember", tags = "客户表管理")
public class BizMemberController {

    private final  BizMemberService bizMemberService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param bizMember 客户表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizmember_get')" )
    public R getBizMemberPage(Page page, BizMember bizMember) {
        return R.ok(bizMemberService.page(page, Wrappers.query(bizMember)));
    }


    /**
     * 通过id查询客户表
     * @param memId id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{memId}" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizmember_get')" )
    public R getById(@PathVariable("memId" ) Long memId) {
        return R.ok(bizMemberService.getById(memId));
    }

    /**
     * 新增客户表
     * @param bizMember 客户表
     * @return R
     */
    @ApiOperation(value = "新增客户表", notes = "新增客户表")
    @SysLog("新增客户表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('cthcm_bizmember_add')" )
    public R save(@RequestBody BizMember bizMember) {
        return R.ok(bizMemberService.save(bizMember));
    }

    /**
     * 修改客户表
     * @param bizMember 客户表
     * @return R
     */
    @ApiOperation(value = "修改客户表", notes = "修改客户表")
    @SysLog("修改客户表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('cthcm_bizmember_edit')" )
    public R updateById(@RequestBody BizMember bizMember) {
        return R.ok(bizMemberService.updateById(bizMember));
    }

    /**
     * 通过id删除客户表
     * @param memId id
     * @return R
     */
    @ApiOperation(value = "通过id删除客户表", notes = "通过id删除客户表")
    @SysLog("通过id删除客户表" )
    @DeleteMapping("/{memId}" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizmember_del')" )
    public R removeById(@PathVariable Long memId) {
        return R.ok(bizMemberService.removeById(memId));
    }

}
