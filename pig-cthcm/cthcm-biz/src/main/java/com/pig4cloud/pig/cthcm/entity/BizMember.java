/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.cthcm.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 客户表
 *
 * @author pig code generator
 * @date 2021-08-16 16:10:15
 */
@Data
@TableName("biz_member")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "客户表")
public class BizMember extends Model<BizMember> {
private static final long serialVersionUID = 1L;

    /**
     * 会员ID
     */
    @TableId
    @ApiModelProperty(value="会员ID")
    private Long memId;
    /**
     * 公司名称
     */
    @ApiModelProperty(value="公司名称")
    private String memName;
    /**
     * 营业执照号
     */
    @ApiModelProperty(value="营业执照号")
    private String memCode;
    /**
     * 企业法人
     */
    @ApiModelProperty(value="企业法人")
    private String legalPerson;
    /**
     * 商铺描述
     */
    @ApiModelProperty(value="商铺描述")
    private String shopDescribe;
    /**
     * 经营范围
     */
    @ApiModelProperty(value="经营范围")
    private String bizScope;
    /**
     * 省份ID
     */
    @ApiModelProperty(value="省份ID")
    private Integer provincesId;
    /**
     * 省份
     */
    @ApiModelProperty(value="省份")
    private String provincesName;
    /**
     * 城市ID
     */
    @ApiModelProperty(value="城市ID")
    private Integer cityId;
    /**
     * 城市
     */
    @ApiModelProperty(value="城市")
    private String cityName;
    /**
     * 区域（县）
     */
    @ApiModelProperty(value="区域（县）")
    private String areaName;
    /**
     * 联系人
     */
    @ApiModelProperty(value="联系人")
    private String linkman;
    /**
     * 详细地址
     */
    @ApiModelProperty(value="详细地址")
    private String addressDetail;
    /**
     * 区域ID
     */
    @ApiModelProperty(value="区域ID")
    private Integer areaId;
    /**
     * 座机号码
     */
    @ApiModelProperty(value="座机号码")
    private String telephone;
    /**
     * 手机号码
     */
    @ApiModelProperty(value="手机号码")
    private String mobilePhone;
    /**
     * 邮箱
     */
    @ApiModelProperty(value="邮箱")
    private String email;
    /**
     * 状态
     */
    @ApiModelProperty(value="状态")
    private Integer status;
    /**
     * 创建人
     */
    @ApiModelProperty(value="创建人")
    private String createBy;
    /**
     * 注册时间
     */
    @ApiModelProperty(value="注册时间")
    private LocalDateTime createTime;
    /**
     * 更新人
     */
    @ApiModelProperty(value="更新人")
    private String updateBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    /**
     * 删除标识
     */
    @ApiModelProperty(value="删除标识")
    private Integer delFlag;
    /**
     * 登录账号
     */
    @ApiModelProperty(value="登录账号")
    private String userName;
    /**
     * 商户管理员用户ID
     */
    @ApiModelProperty(value="商户管理员用户ID")
    private Integer menUserId;
    }
