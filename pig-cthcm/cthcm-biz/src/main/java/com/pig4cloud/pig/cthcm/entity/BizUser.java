/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.cthcm.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 顾客表
 *
 * @author pig code generator
 * @date 2021-08-17 17:52:55
 */
@Data
@TableName("biz_user")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "顾客表")
public class BizUser extends Model<BizUser> {
private static final long serialVersionUID = 1L;

    /**
     * 顾客ID
     */
    @TableId
    @ApiModelProperty(value="顾客ID")
    private Long userId;
    /**
     * 顾客昵称
     */
    @ApiModelProperty(value="顾客昵称")
    private String nickName;
    /**
     * 微信开放ID
     */
    @ApiModelProperty(value="微信开放ID")
    private String openId;
    /**
     * 手机号
     */
    @ApiModelProperty(value="手机号")
    private String phoneNumber;
    /**
     * 注册方式
     */
    @ApiModelProperty(value="注册方式")
    private Integer logonMode;
    /**
     * 注册时间
     */
    @ApiModelProperty(value="注册时间")
    private LocalDateTime createTime;
    /**
     * 性别
     */
    @ApiModelProperty(value="性别")
    private Integer sex;
    /**
     * 头像地址
     */
    @ApiModelProperty(value="头像地址")
    private String avatar;
    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remark;
    /**
     * 注册来源
     */
    @ApiModelProperty(value="注册来源")
    private Long logonSource;
    }
