/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.cthcm.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.cthcm.entity.BizCore;
import com.pig4cloud.pig.cthcm.service.BizCoreService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 核心业务
 *
 * @author pig code generator
 * @date 2021-08-11 11:27:05
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bizcore" )
@Api(value = "bizcore", tags = "核心业务管理")
public class BizCoreController {

    private final  BizCoreService bizCoreService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param bizCore 核心业务
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizcore_get')" )
    public R getBizCorePage(Page page, BizCore bizCore) {
        return R.ok(bizCoreService.page(page, Wrappers.query(bizCore)));
    }


    /**
     * 通过id查询核心业务
     * @param bizId id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{bizId}" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizcore_get')" )
    public R getById(@PathVariable("bizId" ) Integer bizId) {
        return R.ok(bizCoreService.getById(bizId));
    }

    /**
     * 新增核心业务
     * @param bizCore 核心业务
     * @return R
     */
    @ApiOperation(value = "新增核心业务", notes = "新增核心业务")
    @SysLog("新增核心业务" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('cthcm_bizcore_add')" )
    public R save(@RequestBody BizCore bizCore) {
        return R.ok(bizCoreService.save(bizCore));
    }

    /**
     * 修改核心业务
     * @param bizCore 核心业务
     * @return R
     */
    @ApiOperation(value = "修改核心业务", notes = "修改核心业务")
    @SysLog("修改核心业务" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('cthcm_bizcore_edit')" )
    public R updateById(@RequestBody BizCore bizCore) {
        return R.ok(bizCoreService.updateById(bizCore));
    }

    /**
     * 通过id删除核心业务
     * @param bizId id
     * @return R
     */
    @ApiOperation(value = "通过id删除核心业务", notes = "通过id删除核心业务")
    @SysLog("通过id删除核心业务" )
    @DeleteMapping("/{bizId}" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizcore_del')" )
    public R removeById(@PathVariable Integer bizId) {
        return R.ok(bizCoreService.removeById(bizId));
    }

}
