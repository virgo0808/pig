/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pig.cthcm.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pig.common.core.util.R;
import com.pig4cloud.pig.common.log.annotation.SysLog;
import com.pig4cloud.pig.cthcm.entity.BizUser;
import com.pig4cloud.pig.cthcm.service.BizUserService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;


/**
 * 顾客表
 *
 * @author pig code generator
 * @date 2021-08-17 17:52:55
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/bizuser" )
@Api(value = "bizuser", tags = "顾客表管理")
public class BizUserController {

    private final  BizUserService bizUserService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param bizUser 顾客表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizuser_get')" )
    public R getBizUserPage(Page page, BizUser bizUser) {
        return R.ok(bizUserService.page(page, Wrappers.query(bizUser)));
    }


    /**
     * 通过id查询顾客表
     * @param userId id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{userId}" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizuser_get')" )
    public R getById(@PathVariable("userId" ) Long userId) {
        return R.ok(bizUserService.getById(userId));
    }

    /**
     * 新增顾客表
     * @param bizUser 顾客表
     * @return R
     */
    @ApiOperation(value = "新增顾客表", notes = "新增顾客表")
    @SysLog("新增顾客表" )
    @PostMapping
    public R save(@RequestBody BizUser bizUser) {
        bizUser.setCreateTime(LocalDateTime.now());
        return R.ok(bizUserService.save(bizUser));
    }

    /**
     * 修改顾客表
     * @param bizUser 顾客表
     * @return R
     */
    @ApiOperation(value = "修改顾客表", notes = "修改顾客表")
    @SysLog("修改顾客表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('cthcm_bizuser_edit')" )
    public R updateById(@RequestBody BizUser bizUser) {
        return R.ok(bizUserService.updateById(bizUser));
    }

    /**
     * 通过id删除顾客表
     * @param userId id
     * @return R
     */
    @ApiOperation(value = "通过id删除顾客表", notes = "通过id删除顾客表")
    @SysLog("通过id删除顾客表" )
    @DeleteMapping("/{userId}" )
    @PreAuthorize("@pms.hasPermission('cthcm_bizuser_del')" )
    public R removeById(@PathVariable Long userId) {
        return R.ok(bizUserService.removeById(userId));
    }

}
