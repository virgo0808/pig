DROP schema IF EXISTS `cthcm`;
create schema cthcm;

use cthcm;
create table cthcm.ad_detail
(
    ad_id     bigint                    null comment '广告ID',
    mem_id    int                       null comment '会员ID',
    mem_name  varchar(128) charset utf8 null comment '会员名称',
    user_id   int                       null comment '用户ID',
    status    int                       null comment '状态',
    read_time date                      null comment '接收时间',
    constraint ad_detail_pk
        unique (ad_id, mem_id)
)
    comment '广告详情表';

create table cthcm.ad_info
(
    ad_id       bigint auto_increment comment '广告ID'
        primary key,
    ad_name     varchar(258) charset utf8 null comment '广告名',
    ad_intro    varchar(500) charset utf8 null comment '广告简介',
    ad_class_id int                       null comment '广告分类ID',
    ad_class    varchar(128) charset utf8 null comment '广告分类',
    ad_type_id  int                       null comment '广告类型ID',
    ad_type     varchar(128) charset utf8 null comment '广告类型',
    ad_content  text                      null comment '广告内容',
    create_by   varchar(128) charset utf8 null comment '创建者',
    create_time date                      null comment '创建时间',
    update_by   varchar(128) charset utf8 null comment '更新者',
    update_time date                      null comment '更新时间',
    status      int                       null comment '状态',
    del_flag    int                       null comment '删除标识',
    remark      varchar(500) charset utf8 null comment '备注'
)
    comment '广告管理表';

create table cthcm.biz_contract
(
    contract_id       bigint auto_increment comment '合同ID'
        primary key,
    contract_no       varchar(20)               null comment '合同编号',
    contract_title    varchar(128) charset utf8 null comment '合同标题',
    first_party_id    int                       null comment '甲方ID',
    first_party       varchar(128) charset utf8 null comment '甲方',
    second_party_id   int                       null comment '乙方ID',
    second_party      varchar(128) charset utf8 null comment '乙方',
    start_time        date                      null comment '生效时间',
    end_time          date                      null comment '失效时间',
    contract_life     int                       null comment '合同有效时长（天）',
    contract_file     text                      null comment '合同文本',
    fee               decimal                   null comment '费用（元）',
    pay_way           varchar(128) charset utf8 null comment '支付方式',
    pay_way_id        int                       null comment '支付方式id',
    char_fee          varchar(128) charset utf8 null comment '大写费用',
    contract_contents text                      null comment '合同内容',
    create_time       date                      null comment '创建时间',
    creator_id        int                       null comment '创建人ID',
    update_id         int                       null comment '更新人ID',
    sign_date         date                      null comment '签订日期',
    creator           varchar(128) charset utf8 null comment '创建人',
    update_time       date                      null comment '更新时间',
    update_by         varchar(128) charset utf8 null comment '更新人',
    remark            varchar(800) charset utf8 null comment '备注'
)
    comment '业务合同表';

create table cthcm.biz_contract_detail
(
    contract_id int                       null comment '合同ID',
    biz_id      bigint                    null comment '业务ID',
    biz_name    varchar(128) charset utf8 null comment '业务名称',
    constraint biz_contract_detail_pk
        unique (contract_id, biz_id)
)
    comment '合同业务范围表';

create table cthcm.biz_core
(
    biz_id           int auto_increment comment '主键'
        primary key,
    biz_name         varchar(128) charset utf8 null comment '业务名称',
    biz_introduction text                      null comment '业务介绍',
    create_by        varchar(120) charset utf8 null comment '创建人',
    create_time      date                      null comment '创建时间',
    update_by        varchar(120) charset utf8 null comment '更新人',
    update_time      date                      null comment '更新时间',
    status           int                       null comment '状态',
    del_flag         int                       null comment '删除状态'
);

create table cthcm.biz_dict
(
    id          int auto_increment comment '编号'
        primary key,
    type        varchar(100)                       null,
    description varchar(100)                       null,
    create_time datetime default CURRENT_TIMESTAMP not null comment '创建时间',
    update_time datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '更新时间',
    remarks     varchar(255)                       null,
    `system`    char     default '0'               null,
    del_flag    char     default '0'               null
)
    comment '字典表';

create index biz_dict_del_flag
    on cthcm.biz_dict (del_flag);

create table cthcm.biz_dict_item
(
    id          int auto_increment comment '编号'
        primary key,
    dict_id     int                                not null,
    value       varchar(100)                       null,
    label       varchar(100)                       null,
    type        varchar(100)                       null,
    description varchar(100)                       null,
    sort        int      default 0                 not null comment '排序（升序）',
    create_time datetime default CURRENT_TIMESTAMP not null comment '创建时间',
    update_time datetime default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '更新时间',
    remarks     varchar(255)                       null,
    del_flag    char     default '0'               null
)
    comment '字典项';

create index biz_dict_del_flag
    on cthcm.biz_dict_item (del_flag);

create index biz_dict_label
    on cthcm.biz_dict_item (label);

create index biz_dict_value
    on cthcm.biz_dict_item (value);

create table cthcm.biz_member
(
    mem_id         bigint auto_increment comment '会员ID'
        primary key,
    mem_name       varchar(256) charset utf8 null comment '公司名称',
    mem_code       varchar(18)               null comment '营业执照号',
    legal_person   varchar(120) charset utf8 null comment '企业法人',
    shop_describe  varchar(500) charset utf8 null comment '商铺描述',
    biz_scope      varchar(256) charset utf8 null comment '经营范围',
    provinces_id   int                       null comment '省份ID',
    provinces_name varchar(32) charset utf8  null comment '省份',
    city_id        int                       null comment '城市ID',
    city_name      varchar(32) charset utf8  null comment '城市',
    area_name      varchar(32) charset utf8  null comment '区域（县）',
    linkman        varchar(120) charset utf8 null comment '联系人',
    address_detail varchar(128) charset utf8 null comment '详细地址',
    area_id        int                       null comment '区域ID',
    telephone      varchar(12)               null comment '座机号码',
    mobile_phone   varchar(11)               null comment '手机号码',
    email          varchar(32)               null comment '邮箱',
    status         int                       null comment '状态',
    create_by      varchar(120) charset utf8 null comment '创建人',
    create_time    date                      null comment '注册时间',
    update_by      varchar(120) charset utf8 null comment '更新人',
    update_time    date                      null comment '更新时间',
    del_flag       int                       null comment '删除标识',
    user_name      varchar(64)               null comment '登录账号',
    men_user_id    int                       null comment '商户管理员用户ID'
)
    comment '会员表';

create table cthcm.biz_user
(
    user_id      bigint auto_increment comment '顾客ID'
        primary key,
    nick_name    varchar(120) charset utf8 null comment '顾客昵称',
    open_id      varchar(24)               null comment '微信开放ID',
    phone_number varchar(11)               null comment '手机号',
    logon_mode   int                       null comment '注册方式',
    create_time  date                      null comment '注册时间',
    sex          int                       null comment '性别',
    avatar       varchar(128)              null comment '头像地址',
    remark       varchar(500)              null comment '备注'
)
    comment '顾客表';

create table cthcm.cpn_coupon_info
(
    info_id       bigint auto_increment comment '主键'
        primary key,
    coupon_title  varchar(120) charset utf8 null comment '优惠券活动名称',
    coupon_class  int default 0             null comment '优惠券分类',
    coupon_type   int                       null comment '优惠券类型',
    coupon_num    int                       null comment '优惠券数量',
    receive_num   int default 0             null comment '用户领取数量',
    usage_num     int default 0             null comment '用户使用数量',
    face_value    decimal                   null comment '优惠券面值',
    threshold     decimal                   null comment '使用门槛',
    obtain_way    int                       null comment '优惠券获取途径',
    receive_limit int                       null comment '个人领取上限',
    start_time    date                      null comment '生效时间',
    end_time      date                      null comment '失效时间',
    issue_time    date                      null comment '开始发放时间',
    stop_time     date                      null comment '发放停止时间',
    create_time   date                      null comment '活动创建时间',
    create_by     varchar(120) charset utf8 null comment '创建人',
    update_time   date                      null,
    update_by     varchar(120) charset utf8 null,
    status        int                       null comment '状态',
    remark        varchar(500) charset utf8 null comment '备注',
    del_flag      int                       null comment '删除状态',
    create_mem_id bigint                    null comment '创建商户ID',
    biz_id        int                       null comment '业务ID'
)
    comment '优惠券活动';

create table cthcm.cpn_coupon_men
(
    info_id bigint not null comment '优惠券活动ID',
    mem_id  int    not null comment '商户ID',
    constraint cpn_coupon_men_pk
        unique (info_id, mem_id)
)
    comment '优惠券发放范围关系表';

create table cthcm.cpn_coupon_user
(
    cpn_id          bigint auto_increment comment '优惠券ID'
        primary key,
    user_id         bigint                    not null comment '持有用户ID',
    info_id         bigint                    not null comment '优惠活动ID',
    status          int                       null comment '是否使用（0未使用，1已使用）',
    receive_time    date                      null comment '领取时间',
    user_name       varchar(120) charset utf8 null comment '用户姓名',
    verify_time     date                      null comment '使用时间',
    verify_mem_id   int                       null comment '核销商户ID',
    verify_mem_name varchar(120) charset utf8 null comment '核销商户',
    receive_user    varchar(120) charset utf8 null comment '优惠券领取用户',
    coupon_code     varchar(32)               not null comment '优惠券码'
)
    comment '优惠券用户领用表';

create table cthcm.msg_detail
(
    msg_id    bigint                    null comment '消息id',
    mem_id    int                       null comment '会员ID',
    user_id   int                       null,
    mem_name  varchar(128) charset utf8 null comment '会员名称',
    status    int                       null comment '状态（0未读，1已读）',
    read_time date                      null comment '阅读时间',
    constraint msg_mem_detail_pk
        unique (msg_id, mem_id)
)
    comment '消息详情表';

create table cthcm.msg_info
(
    msg_id       bigint auto_increment comment '消息ID'
        primary key,
    msg_title    varchar(256) charset utf8 null comment '消息头',
    msg_type     int                       null comment '消息类型',
    msg_content  text                      null comment '消息内容',
    create_by    varchar(128) charset utf8 null,
    create_time  date                      null comment '创建时间',
    update_by    varchar(128) charset utf8 null comment '更新人',
    update_time  date                      null comment '更新时间',
    remark       varchar(500) charset utf8 null comment '备注',
    release_time date                      null comment '发布时间',
    status       int                       null comment '状态（0未发布，1已发布）',
    del_flag     int                       null comment '删除状态'
)
    comment '消息管理';

create table cthcm.prm_promo_info
(
    promo_id         bigint auto_increment comment '主键'
        primary key,
    promo_title      varchar(120) charset utf8 null,
    promo_type       int                       null comment '秒杀类型',
    start_time       date                      null comment '秒杀开始时间',
    end_time         date                      null comment '秒杀结束时间',
    create_time      date                      null comment '活动创建时间',
    create_by        varchar(120) charset utf8 null comment '活动创建人',
    update_time      date                      null comment '活动更新时间',
    update_by        varchar(120) charset utf8 null comment '更新人',
    status           int                       null comment '状态',
    promo_item_price decimal                   null comment '秒杀物品价格',
    item_id          bigint                    null comment '秒杀物品ID',
    item_num         int                       null comment '秒杀物品数量',
    del_flag         int                       null comment '删除状态'
)
    comment '秒杀活动表';
commit ;

-- cthcm.ad_detail definition

CREATE TABLE `ad_detail` (
  `ad_id` bigint DEFAULT NULL COMMENT '广告ID',
  `mem_id` int DEFAULT NULL COMMENT '会员ID',
  `mem_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `user_id` int DEFAULT NULL COMMENT '用户ID',
  `status` int DEFAULT NULL COMMENT '状态',
  `read_time` date DEFAULT NULL COMMENT '接收时间',
  UNIQUE KEY `ad_detail_pk` (`ad_id`,`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='广告详情表';


-- cthcm.ad_info definition

CREATE TABLE `ad_info` (
  `ad_id` bigint NOT NULL AUTO_INCREMENT COMMENT '广告ID',
  `ad_name` varchar(258) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '广告名',
  `ad_intro` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '广告简介',
  `ad_class_id` int DEFAULT NULL COMMENT '广告分类ID',
  `ad_class` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '广告分类',
  `ad_type_id` int DEFAULT NULL COMMENT '广告类型ID',
  `ad_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '广告类型',
  `ad_content` text COMMENT '广告内容',
  `create_by` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建者',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新者',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  `status` int DEFAULT NULL COMMENT '状态',
  `del_flag` int DEFAULT NULL COMMENT '删除标识',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='广告管理表';


-- cthcm.biz_contract definition

CREATE TABLE `biz_contract` (
  `contract_id` bigint NOT NULL AUTO_INCREMENT COMMENT '合同ID',
  `contract_no` varchar(20) DEFAULT NULL COMMENT '合同编号',
  `contract_title` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '合同标题',
  `first_party_id` int DEFAULT NULL COMMENT '甲方ID',
  `first_party` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '甲方',
  `second_party_id` int DEFAULT NULL COMMENT '乙方ID',
  `second_party` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '乙方',
  `start_time` date DEFAULT NULL COMMENT '生效时间',
  `end_time` date DEFAULT NULL COMMENT '失效时间',
  `contract_life` int DEFAULT NULL COMMENT '合同有效时长（天）',
  `contract_file` text COMMENT '合同文本',
  `fee` decimal(10,0) DEFAULT NULL COMMENT '费用（元）',
  `pay_way` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '支付方式',
  `pay_way_id` int DEFAULT NULL COMMENT '支付方式id',
  `char_fee` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '大写费用',
  `contract_contents` text COMMENT '合同内容',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `creator_id` int DEFAULT NULL COMMENT '创建人ID',
  `update_id` int DEFAULT NULL COMMENT '更新人ID',
  `sign_date` date DEFAULT NULL COMMENT '签订日期',
  `creator` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `remark` varchar(800) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='业务合同表';


-- cthcm.biz_contract_detail definition

CREATE TABLE `biz_contract_detail` (
  `contract_id` int DEFAULT NULL COMMENT '合同ID',
  `biz_id` bigint DEFAULT NULL COMMENT '业务ID',
  `biz_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '业务名称',
  UNIQUE KEY `biz_contract_detail_pk` (`contract_id`,`biz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='合同业务范围表';


-- cthcm.biz_core definition

CREATE TABLE `biz_core` (
  `biz_id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `biz_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '业务名称',
  `biz_introduction` text COMMENT '业务介绍',
  `create_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  `status` int DEFAULT NULL COMMENT '状态',
  `del_flag` int DEFAULT NULL COMMENT '删除状态',
  PRIMARY KEY (`biz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='核心业务';


-- cthcm.biz_dict definition

CREATE TABLE `biz_dict` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL,
  `system` char(1) DEFAULT '0',
  `del_flag` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `biz_dict_del_flag` (`del_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典表';


-- cthcm.biz_dict_item definition

CREATE TABLE `biz_dict_item` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '编号',
  `dict_id` int NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `label` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序（升序）',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL,
  `del_flag` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `biz_dict_del_flag` (`del_flag`),
  KEY `biz_dict_label` (`label`),
  KEY `biz_dict_value` (`value`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='字典项';


-- cthcm.biz_member definition

CREATE TABLE `biz_member` (
  `mem_id` bigint NOT NULL AUTO_INCREMENT COMMENT '会员ID',
  `mem_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '公司名称',
  `mem_code` varchar(18) DEFAULT NULL COMMENT '营业执照号',
  `legal_person` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '企业法人',
  `shop_describe` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '商铺描述',
  `biz_scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '经营范围',
  `provinces_id` int DEFAULT NULL COMMENT '省份ID',
  `provinces_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '省份',
  `city_id` int DEFAULT NULL COMMENT '城市ID',
  `city_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '城市',
  `area_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '区域（县）',
  `linkman` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系人',
  `address_detail` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细地址',
  `area_id` int DEFAULT NULL COMMENT '区域ID',
  `telephone` varchar(12) DEFAULT NULL COMMENT '座机号码',
  `mobile_phone` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(32) DEFAULT NULL COMMENT '邮箱',
  `status` int DEFAULT NULL COMMENT '状态',
  `create_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` date DEFAULT NULL COMMENT '注册时间',
  `update_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  `del_flag` int DEFAULT NULL COMMENT '删除标识',
  `user_name` varchar(64) DEFAULT NULL COMMENT '登录账号',
  `men_user_id` int DEFAULT NULL COMMENT '商户管理员用户ID',
  `longitude` decimal(10,0) DEFAULT NULL COMMENT '经度',
  `latitude` decimal(10,0) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户表';


-- cthcm.biz_user definition

CREATE TABLE `biz_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '顾客ID',
  `nick_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '顾客昵称',
  `open_id` varchar(24) DEFAULT NULL COMMENT '微信开放ID',
  `phone_number` varchar(11) DEFAULT NULL COMMENT '手机号',
  `logon_mode` int DEFAULT NULL COMMENT '注册方式',
  `create_time` date DEFAULT NULL COMMENT '注册时间',
  `sex` int DEFAULT NULL COMMENT '性别',
  `avatar` varchar(128) DEFAULT NULL COMMENT '头像地址',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `logon_source` bigint DEFAULT NULL COMMENT '注册来源',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='顾客表';


-- cthcm.cpn_coupon_info definition

CREATE TABLE `cpn_coupon_info` (
  `info_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `coupon_title` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠券活动名称',
  `coupon_class` int DEFAULT '0' COMMENT '优惠券分类',
  `coupon_type` int DEFAULT NULL COMMENT '优惠券类型',
  `coupon_num` int DEFAULT NULL COMMENT '优惠券数量',
  `receive_num` int DEFAULT '0' COMMENT '用户领取数量',
  `usage_num` int DEFAULT '0' COMMENT '用户使用数量',
  `face_value` decimal(10,0) DEFAULT NULL COMMENT '优惠券面值',
  `threshold` decimal(10,0) DEFAULT NULL COMMENT '使用门槛',
  `obtain_way` int DEFAULT NULL COMMENT '优惠券获取途径',
  `receive_limit` int DEFAULT NULL COMMENT '个人领取上限',
  `start_time` date DEFAULT NULL COMMENT '生效时间',
  `end_time` date DEFAULT NULL COMMENT '失效时间',
  `issue_time` date DEFAULT NULL COMMENT '开始发放时间',
  `stop_time` date DEFAULT NULL COMMENT '发放停止时间',
  `create_time` date DEFAULT NULL COMMENT '活动创建时间',
  `create_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建人',
  `update_time` date DEFAULT NULL,
  `update_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `status` int DEFAULT NULL COMMENT '状态',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `del_flag` int DEFAULT NULL COMMENT '删除状态',
  `create_mem_id` bigint DEFAULT NULL COMMENT '创建商户ID',
  `biz_id` int DEFAULT NULL COMMENT '业务ID',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券活动';


-- cthcm.cpn_coupon_men definition

CREATE TABLE `cpn_coupon_men` (
  `info_id` bigint NOT NULL COMMENT '优惠券活动ID',
  `mem_id` int NOT NULL COMMENT '商户ID',
  UNIQUE KEY `cpn_coupon_men_pk` (`info_id`,`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券发放范围关系表';


-- cthcm.cpn_coupon_user definition

CREATE TABLE `cpn_coupon_user` (
  `cpn_id` bigint NOT NULL AUTO_INCREMENT COMMENT '优惠券ID',
  `user_id` bigint NOT NULL COMMENT '持有用户ID',
  `info_id` bigint NOT NULL COMMENT '优惠活动ID',
  `status` int DEFAULT NULL COMMENT '是否使用（0未使用，1已使用）',
  `receive_time` date DEFAULT NULL COMMENT '领取时间',
  `user_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户姓名',
  `verify_time` date DEFAULT NULL COMMENT '使用时间',
  `verify_mem_id` int DEFAULT NULL COMMENT '核销商户ID',
  `verify_mem_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '核销商户',
  `receive_user` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '优惠券领取用户',
  `coupon_code` varchar(32) NOT NULL COMMENT '优惠券码',
  PRIMARY KEY (`cpn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='优惠券用户领用表';


-- cthcm.msg_detail definition

CREATE TABLE `msg_detail` (
  `msg_id` bigint DEFAULT NULL COMMENT '消息id',
  `mem_id` int DEFAULT NULL COMMENT '会员ID',
  `user_id` int DEFAULT NULL,
  `mem_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '会员名称',
  `status` int DEFAULT NULL COMMENT '状态（0未读，1已读）',
  `read_time` date DEFAULT NULL COMMENT '阅读时间',
  UNIQUE KEY `msg_mem_detail_pk` (`msg_id`,`mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='消息详情表';


-- cthcm.msg_info definition

CREATE TABLE `msg_info` (
  `msg_id` bigint NOT NULL AUTO_INCREMENT COMMENT '消息ID',
  `msg_title` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '消息头',
  `msg_type` int DEFAULT NULL COMMENT '消息类型',
  `msg_content` text COMMENT '消息内容',
  `create_by` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
  `release_time` date DEFAULT NULL COMMENT '发布时间',
  `status` int DEFAULT NULL COMMENT '状态（0未发布，1已发布）',
  `del_flag` int DEFAULT NULL COMMENT '删除状态',
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='消息管理';


-- cthcm.prm_promo_info definition

CREATE TABLE `prm_promo_info` (
  `promo_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `promo_title` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `promo_type` int DEFAULT NULL COMMENT '秒杀类型',
  `start_time` date DEFAULT NULL COMMENT '秒杀开始时间',
  `end_time` date DEFAULT NULL COMMENT '秒杀结束时间',
  `create_time` date DEFAULT NULL COMMENT '活动创建时间',
  `create_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '活动创建人',
  `update_time` date DEFAULT NULL COMMENT '活动更新时间',
  `update_by` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新人',
  `status` int DEFAULT NULL COMMENT '状态',
  `promo_item_price` decimal(10,0) DEFAULT NULL COMMENT '秒杀物品价格',
  `item_id` bigint DEFAULT NULL COMMENT '秒杀物品ID',
  `item_num` int DEFAULT NULL COMMENT '秒杀物品数量',
  `del_flag` int DEFAULT NULL COMMENT '删除状态',
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='秒杀活动表';